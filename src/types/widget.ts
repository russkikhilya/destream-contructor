export interface IWidget {
    widgetTitle: string;
    widgetDescription: string;
    widgetWidth: string;
    widgetHeight: string;
}