import { Component } from '@angular/core';
import { exportHtmlContent } from '../../utils/componentextensions'

@Component({
  selector: 'export-comp',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent {
    export(): void {
        const widgetsRegion = document.getElementById("widgets-region");
        if (widgetsRegion) {
            const copyWidgetsRegion = widgetsRegion.cloneNode(true) as HTMLElement;
            exportHtmlContent(copyWidgetsRegion);
        }
    }
}