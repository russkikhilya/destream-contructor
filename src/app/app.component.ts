import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { AddedWidgetComponent } from './addwidget-component/addedwidget.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  widgetTitle: string = "";
  widgetDescription: string = "";
  widgetFontSize: string = "";
  widgetWidth: string = "";
  widgetHeight: string = "";
  widgetsContainer: any;
  addWidgetWidth: string = "";
  addWidgetHeight: string = "";
  createdComponent: any = null;
  focusedComponent: any = null;
  tabIndex: number = 0;

  @ViewChild('container', {read: ViewContainerRef})
  container: ViewContainerRef | undefined;
  
  constructor(private resolver: ComponentFactoryResolver) {
  }

  ngOnInit() {
    this.widgetsContainer = document.getElementById("widgets-region");
    this.addWidgetWidth = this.widgetsContainer.getBoundingClientRect().width;
    this.addWidgetHeight = this.widgetsContainer.getBoundingClientRect().height;
  }

  onInputTitle(event: any): void {
    if (this.createdComponent) {
      this.createdComponent.instance.widgetTitle = event.target.value;
    } else if (this.focusedComponent) {
      this.focusedComponent.widgetTitle = event.target.value;
    }
  }

  onInputDescription(event: any): void {
    if (this.createdComponent) {
      this.createdComponent.instance.widgetDescription = event.target.value;
    } else if (this.focusedComponent) {
      this.focusedComponent.widgetDescription = event.target.value;
    }
  }
  
  onInputFont(event: any): void {
    if (event.target.value > 60) {
      this.widgetFontSize = "60";
      event.target.value = this.widgetFontSize;
    } else {
      this.widgetFontSize = event.target.value;
    }
    if (this.createdComponent) {
      this.createdComponent.instance.fontSize = this.widgetFontSize;
    } else if (this.focusedComponent) {
      this.focusedComponent.fontSize = this.widgetFontSize;
    }
  }

  onInputWidth(event: any): void {
    if (event.target.value > this.addWidgetWidth) {
      this.widgetWidth = (parseInt(this.addWidgetWidth) - 10).toString();
      event.target.value = this.widgetWidth;
    } else {
      this.widgetWidth = event.target.value;
    }
    if (this.createdComponent) {
      this.createdComponent.instance.widgetWidth = this.widgetWidth;
    } else if (this.focusedComponent) {
      this.focusedComponent.widgetWidth = this.widgetWidth;
    }
  }

  onInputHeight(event: any): void {
    if (event.target.value > this.addWidgetHeight) {
      this.widgetHeight = (parseInt(this.addWidgetHeight) - 10).toString();
      event.target.value = this.widgetHeight;
    } else {
      this.widgetHeight = event.target.value;
    }
    if (this.createdComponent) {
      this.createdComponent.instance.widgetHeight = this.widgetHeight;
    } else if (this.focusedComponent) {
      this.focusedComponent.widgetHeight = this.widgetHeight;
    }
  }

  addWidget(): void {
    this.renderCreatedComponent();
  }

  renderCreatedComponent(): void {
    this.flushInputs();
    const myDynamicComponentFactory = this.resolver.resolveComponentFactory(AddedWidgetComponent);
    if (this.container) {
      this.createdComponent = this.container.createComponent(myDynamicComponentFactory);
      this.createdComponentInit(this.createdComponent);
      this.tabIndex++;
    }
  }

  flushInputs(): void {
    this.widgetTitle = "";
    this.widgetDescription = "";
    this.widgetFontSize = "";
    this.widgetWidth = "";
    this.widgetHeight = "";
  }

  focusComponent(component: any): void {
    this.createdComponent = null;
    this.focusedComponent = component;
    this.widgetTitle = this.focusedComponent.widgetTitle;
    this.widgetDescription = this.focusedComponent.widgetDescription;
    this.widgetFontSize = this.focusedComponent.fontSize;
    this.widgetWidth = this.focusedComponent.widgetWidth;
    this.widgetHeight = this.focusedComponent.widgetHeight;
  }

  resizeWidget(width: string, height: string): void {
    this.widgetWidth = width;
    this.widgetHeight = height;
  }

  createdComponentInit(createdComponent: any): void {
    createdComponent.instance.tabIndex = this.tabIndex;
    createdComponent.instance.widgetTitle = "";
    createdComponent.instance.widgetDescription = "";
    createdComponent.instance.widgetFontSize = "";
    createdComponent.instance.widgetWidth = "";
    createdComponent.instance.widgetHeight = "";
    createdComponent.instance.parentComponent = this;
  }
}
