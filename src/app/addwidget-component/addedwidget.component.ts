import { Component, AfterViewInit } from '@angular/core';
import { IWidget } from 'src/types/widget';

@Component({
  selector: 'add-widget-comp',
  templateUrl: './addedwidget.component.html',
  styleUrls: ['./addedwidget.component.css']
})
export class AddedWidgetComponent implements AfterViewInit, IWidget {
    widgetTitle: string = "";
    widgetDescription: string = "";
    fontSize: string = "";
    widgetWidth: string = "";
    widgetHeight: string = "";
    tabIndex: number = 0;
    parentComponent: any;
    
    ngAfterViewInit() {
        const addedWidget = document.getElementById(this.tabIndex.toString());
        if (addedWidget) {
            addedWidget.style.zIndex = "10000";
            this.addEventListenersToNewWidget(addedWidget);
        }
    }

    addEventListenersToNewWidget(widget: HTMLElement): void {
        widget.style.zIndex = "10000";
        widget.onmousedown = (e) => this.onMouseDown(e, widget);
        widget.onfocus = (e) => this.onFocusWidget(e, widget);
        widget.onblur = (e) => this.onBlurWidget(e, widget);
        const resizer = widget.querySelector(".resizer") as HTMLElement;
        if (resizer) {
            resizer.onmousedown =(e) => this.mousedownResizer(e, widget);
        }
    }

    mousedownResizer(event: any, draggableElement: any): void {
        event.stopPropagation();
        let prevX = event.clientX;
        let prevY = event.clientY;
        const resizeWidgetFunc = (width: string, height: string) => this.parentComponent.resizeWidget(width, height);
        const addWidgetRef = this;
        window.addEventListener("mousemove", mousemove);
        window.addEventListener("mouseup", mouseup);

        function mousemove(e: any) {
            const parentElRect = draggableElement.parentNode.parentNode.getBoundingClientRect();
            const draggableElRect = draggableElement.getBoundingClientRect();
            if (parentElRect.bottom >= e.clientY && parentElRect.right >= e.clientX) {
                const draggableElementWidth = draggableElRect.width - (prevX - e.clientX);
                const draggableElementHeight = draggableElRect.height - (prevY - e.clientY);
                const parentWidth = parentElRect.width;
                const parentHeight = parentElRect.height;
                const calculatedWidth = draggableElementWidth > parentWidth ? parentWidth - 10 : draggableElementWidth;
                const calculatedHeight = draggableElementHeight > parentHeight ? parentHeight - 10 : draggableElementHeight;
                
                draggableElement.style.width = calculatedWidth + "px";
                draggableElement.style.height = calculatedHeight + "px";
    
                prevX = e.clientX;
                prevY = e.clientY;
                addWidgetRef.widgetWidth = calculatedWidth.toString();
                addWidgetRef.widgetHeight = calculatedHeight.toString();
                resizeWidgetFunc(addWidgetRef.widgetWidth, addWidgetRef.widgetHeight);
            }
        }
        function mouseup() {
            window.removeEventListener("mousemove", mousemove);
            window.removeEventListener("mouseup", mouseup);
        }
    }

    onMouseDown(event: any, draggableElement: any): void {
        const offsetX = event.pageX - draggableElement.offsetLeft;
        const offsetY = event.pageY - draggableElement.offsetTop;
        document.onmousemove = (e) => {
            draggableElement.style.left = Math.max(Math.min(e.pageX - offsetX, draggableElement.parentNode.parentNode.clientWidth - draggableElement.clientWidth), 0) + "px";
            draggableElement.style.top = Math.max(Math.min(e.pageY - offsetY, draggableElement.parentNode.parentNode.clientHeight - draggableElement.clientHeight), 0) + "px";
        }
        draggableElement.onmouseup = () => {
            document.onmousemove = null;
            draggableElement.onmouseup = null;
        }
        draggableElement.ondragstart = () => 0;
    }

    onFocusWidget(event: any, element: any): void {
        element.style.border = "1px solid white";
        element.style.zIndex = "10000";
        this.parentComponent.focusComponent(this);
    }

    onBlurWidget(event: any, element: any): void {
        element.style.zIndex = "1000";
        element.style.border = "none";
    }
}
