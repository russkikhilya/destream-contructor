export function exportHtmlContent(htmlContent: any): void {
  prepareHtmlContent(htmlContent);
  const link = document.createElement("a");
  link.setAttribute("download", "widgets.html");
  link.setAttribute("href", "data:" + "text/plain" + ";charset=utf-8," + encodeURIComponent(htmlContent.innerHTML));
  link.click(); 
}

function prepareHtmlContent(content: any): void {
  removeElementsBySelector(content, ".resizer");
  setClassForChildNodes(content.childNodes);
}

function setCssRule(element: any): void {
  const htmlElement = element as HTMLElement;
  if (htmlElement && htmlElement.className) {
      const result = findCssRule(htmlElement.className);
      htmlElement.style.cssText = result + htmlElement.style.cssText;
  }
}

function setClassForChildNodes(nodes: any): void {
  for (let i = 0; i < nodes.length; i++) {
      setCssRule(nodes[i]);
      setClassForChildNodes(nodes[i].childNodes);
  }
}

function removeElementsBySelector(element: any, selector: string): void {
  const elements = element.querySelectorAll(selector);
  if (elements) {
      const resizerLength = elements.length;
      for (let i = 0; i < resizerLength; i++) {
          elements[i].remove();
      }
  }
}

function findCssRule(name: string): any {
  const cssRules = document.styleSheets[0].cssRules;
  for (let i = 0; i <= cssRules.length; i++) {
      const cssRule = cssRules[i] as CSSStyleRule;
      if (cssRule) {
          if (cssRule.selectorText == "." + name) {
              const result = cssRule.cssText;
              return result.substring(result.indexOf('{') + 2, result.indexOf('}') - 1);
          }
      }
  }
}